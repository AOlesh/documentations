---
sidebar_position: 2
---

# Flow Editor - basic

## Flow editor
[Flow Editor](./intro) gives you the ability to design powerful workflows that can integrate a wide range of external systems and create complex logic for your application. In a single workspace, you can have multiple Flow Editor instances which you can combine or make as separate microservices. 

As a base for [Flow Editor](./intro) is used open-source system Node-red. [Node-RED](https://nodered.org/) is a visual tool created by [IBM Emerging Technology](https://developer.ibm.com/components/node-red/?mhsrc=ibmsearch_a&mhq=node-red) and the open source community for developing APIs, hardware devices (IoT) and different online services. You can create complex flow with wide range of nodes and modules (3100+) and extend flow with any [Node.js](https://docs.npmjs.com/) packages available in public repository (225,000) or create your custom node.

## Creating new Flow service
To create new [Flow Editor](./intro) you need to open **Service manager** and click on + button on the row with name **Flow Builder** after that you will see the modal window in which you can write the name for the new service and select a version.

After all fields are completed click on **Create NodeRed** button and UBOS platform will start the process of establishing a new service for you, on average it may take _20-60 sec_. until the service will be available for you, before that service will be in pending mode and you can find it in the **Flow Builder** drop down on the **Service**

![create flow builder](../img/flow-editor-basic-1.gif)

After the new service became available, it will be opened in the **Main Work Zone**.
![flow editor start screen](../img/flow-editor-basic1.jpg)

Once the service is opened you can start work on designing any flow and integration for your app.

## Flow Editor Layout
![flow edotor layout](../img/flow-editor-basic-2.png)

Basic Layout of **Flow Editor** consist of 4 main areas:

- **Header** - Here you can found different options of how to deploy your flow and main menu of Flow Editor
- **Palette** - Contains all available nodes to use
- **Workspace** - Area where flows are created
- **Sidebar** - Contains tabs that provide a wide range of useful tools within the editor

## Creating first HTTP Endpoint

In the beginner tutorial we will create a simple `HTTP Endpoint` that will return to us static text "Hello World!". Let's drag some nodes onto the workspace from the palette.

First you need to find **HTTP In** node, drag it onto the workspace and open node settings by **double click**

![add http in node](../img/flow-editor-basic-3.gif)

**HTTP In** node will listen for requests. In settings window you need to select **method** it will be `GET` method and set a name of the endpoint to `/hello` in the `URL` field, after that click on the **Done** button in the settings window.

![http in node](../img/flow-editor-basic-3.png)

Next let's find a **Template** node, drag it onto the workspace and connect it with the **HTTP In** node.

![add template](../img/flow-editor-basic-4.gif)

To set the static text that endpoint will return you need to open settings of the **Template** node (double click on the node) and change the text in the **Template** field to `"Hello World!"`, to save your changes click on the **Done** button.

![add text to template](../img/flow-editor-basic-5.gif)

At the end our **Flow** need to reply to request with the data from **Template** node, to do that let's find `HTTP Response` node and connect it to the **Template** node(mark number 1). 

Your **Flow** should looks like that:

![flow](../img/flow-editor-basic-4.png)

Now you need to deploy your **Flow** to make it available, to do that click on **Deploy** button in the **Header** of the **Flow Editor** (mark number 2)

## Connecting API to HTTP Endpoint

To connect **API** to `HTTP Endpoint` let's switch the editor to [UI Editor](../ui-editor/ui-editor-basic), you can do that by clicking on the [UI Editor](../ui-editor/ui-editor-basic) tab on **Service** panel. In UI Editor you need to open a list of available **API**s for the current page by clicking on the **API**S row and clicking on `API1` from the list to open the **API pane**.

In case you don't see `API1` in the list, you can create a new one by clicking on the + button on the **API**S row

In the **API** pane, you need to select **Flow services** `flow1` to which you want to connect the current **API** and in the next dropdown select `HTTP Endpoint` with the name `/hello` that we have created. After that system will automatically generate a `URL` to `HTTP Endpoint` on your **Flow**.

![add API](../img/flow-editor-basic-6.gif)

To test your `HTTP Endpoint` click on the **Run button** and you should see the response in `Response body` field.

To close the **API pane** click on **Back** button

## Displaying data on UI
To display data on **UI** we need to have 2 widgets, one widget will be a **Button widget** that will run the `API1`, and the second will be the **Input widget** which will display the response.

You can add a new **Button widget** to the Page editor or use some that already exist. Let's open the **Widget Settings** window of the **Button widget** and at the bottom of the window will be setting section `Actions` and property with name `onClick`, click on that field and you will see a list of available actions.

![](../img/flow-editor-basic-7.gif)

In our case we need action `Execute a query`, click on it and you will see the list of available APIs. From the list choose our **API** that we have created `API1`.

![](../img/flow-editor-basic-5.png)

After you choose `API1`  you should see that the **onClick** property has changed. So after that changes when you will click on that button it will run our `API1` which will make a `HTTP Request` to our **Flow1**

![](../img/flow-editor-basic-6.png)

Now let's display response data from `API1` in the **Input widget**, to do that you need to drag new **Input widget** from **Widget pane** to **Page editor** and open **Widget Settings** window.
![](../img/flow-editor-basic-8.gif)

In the **Widget Settings** window find the `Default Text` property, in it you can write static text or add dynamic data. We need to add a dynamic that will show response data from `API1`. To do that you need to write a command inside the `Default Text` property, remember that each command should be between `{{ }}`. 

The command will look like that: `{{Api1.data}}`
![](../img/flow-editor-basic-7.png)

## Deploy

Now let's **Deploy** the **UI application** and check the changes that we have made. Try to click on the button that you have configured and look at what data will appear in the input that should display data.

![](../img/flow-editor-basic-9.gif)
