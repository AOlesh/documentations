---
sidebar_position: 1
---

# Контроль версій з використанням Git

Використання Git дозволяє контролювати версії програм, можливе приєднання всіх основник сервісів Git (Github, GitLab, Bitbucket), що полегшує відстеження змін, створення відкатів або співпрацю за допомогою гілок.

:::info
Контроль версій працює з будь-якою службою хостингу Git, яка підтримує протокол SSH.
:::


Щоб розпочати використовувати переваги які надає система контролю версій вам потрібно приєднати свій Git репозиторій до вашого додатку на платформі UBOS
