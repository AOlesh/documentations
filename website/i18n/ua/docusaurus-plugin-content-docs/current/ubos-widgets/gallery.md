---
sidebar_position: 10
---


# Gallery

Віджет **Gallery** - це  React Grid галерея яка створена для сучасних браузерів, тому підтримка IE обмежена IE 11 і новішими версіями.

Дуже маленькі мініатюри можуть бути не найестетичнішими тому ууло вибрано розумну висоту рядків за замовчуванням 180 пікселів, але висота рядків до 100 пікселів все ще прийнятна. Ширина галереї визначається вмістом елемента

Параметри зображення: `thumbnail` може вказувати на той самий ресурс, що й `src`, беручи до уваги результуючий розмір даних галереї та вартість завантаження сторінки. `thumbnail` будь-якого розміру будуть масштабовані відповідно до `rowHeight`

Щоб додати віджет на канвас, перетягніть віджет **Gallery** з панелі віджетів, розміщеної ліворуч.

![](./img/gallery-1.gif)

## Властивості

Властивості дозволяють редагувати віджет, з’єднувати його з іншими віджетами та налаштовувати дії користувача.

### Властивості віджета

Ці властивості дозволяють редагувати віджет **Gallery**. Усі властивості присутні на панелі властивостей та перераховані у наведеній нижче таблиці.

 За замовчванням **Gallery** має три картинки.  

#### Chart Data
Віджет приймає масив об'єктів для відображення елементів в діаграмі. Можна отримати дані з існуючого `API` використовуючи `{{}}`

| Chart Data | Опис |
|---|---|
| **`thumbnail`** | URL мініатюри|
| **`thumbnailWidth`** | ширина мініатюри в `px` |
| **`thumbnailHeight`** | висота мініатюри в `px` |
| **`tags`** | Тег який відображється на мініатюрі має додаткові властивості `value` і `title` |
| **`caption`** | Підпис який буде відображатися при наведенні на мініатюру |

![](./img/gallery-1.png)

<details>
  <summary>Переглянути Gallery Data</summary>
  <div>

    [
        {
        "thumbnail": "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_n.jpg",
        "thumbnailWidth": 160,
        "thumbnailHeight": 87,
        "caption": "After Rain (Jeshu John - designerspics.com)"
        },
        {
        "thumbnail": "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_n.jpg",
        "thumbnailWidth": 160,
        "thumbnailHeight": 106,
        "tags": [
            {
            "value": "Ocean",
            "title": "rem"
            },
            {
            "value": "People",
            "title": "People"
            }
        ],
        "caption": "Boats (Jeshu John - designerspics.com)"
        },
        {
        "thumbnail": "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        "thumbnailWidth": 160,
        "thumbnailHeight": 106
        }
    ]
    
  </div>
</details>


| Властивість   | Опис                                                    |
|---------------|---------------------------------------------------------|
|`Custom Is Loading`| |
|`Animate Loading`| Анімація при завантаженні |
|`Custom RowHeight` | Задає специфічну висоту радка. Вимкнена за замовчуванням |
|`Animate Loading`|  |
|`Visible`|Видимість віджета на задеплоєній сторінці|
|`classStyle`| Задає CSS стилі|

### Події 

Ви можете додавати поді написані на `JS` які будуть виконуватися при натисканні на елемент або обирати події з запропонованого списку 

| Подія | Опис |
|--|--|
| `onImgClick` | Викликається подія коли відбувається `click` на мініатюрі галереї|

#### Список подій

Переглянути опис подій які доступні можна в документації [Appsmith](https://docs.appsmith.com/reference/appsmith-framework/widget-actions)