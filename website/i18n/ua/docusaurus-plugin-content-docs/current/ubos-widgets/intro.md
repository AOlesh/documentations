---
sidebar_position: 1
---

# UBOS widgets

Віджети допомагають користувачеві створювати макет програми. Користувачі можуть зберігати дані з бази даних або виклику `API`, запускати події тощо. Віджети можна перетягувати з панелі віджетів, розміщувати на канвасі та змінювати розмір відповідно до даних, які вони повинні відображати. Вони також мають властивості, які можна  редагувати не використовуючи програмний код, щоб налаштувати їхні дані, змінити їхні стилі та ініціювати дії з них.

Платформа **UBOS** використовує віджети власної розробки та віджети розроблені іншими ккомпаніями.
Детальніше про віджети **Appsmith** можна дізнатися [тут](https://docs.appsmith.com/reference/widgets)

### Перелік власних віджетів UBOS:

- [accordion](./accordion)
- [barchart](./barchart)
- [bidirectional chart](./bidirectional-chart)
- [carousel](./carousel)
- [cropper](./cropper)
- [gallery](./gallery)
- [large area chart](./large-area-chart)
- [list collapse](./list-collapse)
- [map with cluster marker](./map-with-cluster-marker)
- [mockup image](./mockup-image)
- [nested table](./nested-table) 
- [socket table](./socket-table) 
- [pie chart](./pie-chart)
- [slider](./slider)
- [stacked column chart](./stacked-column-chart)
- [tree](./tree)
- [word cloud](./word-cloud)
- [function](./function)
- [file manager](./file-manager)

Віджети в активній розробці
- [grid container](./grid-container)
- [row container](./row-container)
- [cell container](./cell-container)
