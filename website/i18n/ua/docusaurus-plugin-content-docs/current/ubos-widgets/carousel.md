---
sidebar_position: 5
---

# Carousel

Віджет **Carousel** представляє собою карусель з декількома слайдами. Віджет має кнопку `вперед` та кнопку `назад` 

Щоб додати віджет **Carousel** на канвас, перетягніть віджет з панелі віджетів, розміщеної ліворуч.

![](./img/carousel-1.gif)

## Властивості

Властивості дозволяють редагувати віджет, з’єднувати його з іншими віджетами та налаштовувати дії користувача.

### Властивості віджета

Ці властивості дозволяють редагувати віджет «Carousel». Усі ці властивості присутні на панелі властивостей віджета. У наведеній нижче таблиці перераховано всі властивості віджета.

 За замовчванням **Carousel** містить шість картинок  

``` js 
[
  {
    "img": "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
  },
  {
    "img": "https://user.oc-static.com/files/6001_7000/6410.jpg"
  },
  {
    "img": "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
  },
  {
    "img": "https://user.oc-static.com/files/6001_7000/6410.jpg"
  },
  {
    "img": "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
  },
  {
    "img": "https://user.oc-static.com/files/6001_7000/6410.jpg"
  }
]
```

| Властивості | Опис |
|---|---|
| visibleSlides | Задає кількість картинок яка одночасно відображається в слайдері|
| Border Color | Задає колір обрамлення віджета  |
| classStyle | Дозволяє підключати стилі CSS |
| Animate Loading| Дозволяє контролювати завантаженням віджета |

### Події 

Ви можете додавати поді написані на `JS` які будуть виконуватися при натисканні на елемент або обирати події з запропонованого списку 

| Подія | Опис |
|--|--|
| `onClick` | Викликається подія коли натискається головний елемент |

#### Список подій які доступні 

![](./img/accordion-1.png)

- No action
- Execute a query
- Execute a JS function
- Navigate to
- Show message
- Open modal
- Close modal
- Store value
- Download
- Copy to clipboard
- Reset widget
- Set interval
- Clear interval
- Get Geolocation
- Watch Geolocation
- Stop watching Geolocation