---
sidebar_position: 2
---

# Accordion

Віджет **Accordion** представляє набір згрупованих категорій. Група може мати прості елементи або елементи меню, що розкриваються.

Щоб додати віджет **Accordion** на канвас, перетягніть віджет з панелі віджетів, розміщеної ліворуч.

![](./img/accordion-1.gif)

## Властивості

Властивості дозволяють редагувати віджет, з’єднувати його з іншими віджетами та налаштовувати дії користувача.

### Властивості віджета

Ці властивості дозволяють редагувати віджет **Accordion**. Усі властивості присутні на панелі властивостей та перераховані у наведеній нижче таблиці.

 За замовчванням **Accordion** містить дві категорії з підкатегорією в кожній  

``` js 
[ 
 { 
    "label": "Category1",
    "value": "1231",
    "children": [
      {
        "label": "SubCategory1",
        "value": "555",
        "children": []
      }
    ]
  },
  {
    "label": "Category2",
    "value": "123",
    "children": [
      {
        "label": "SubCategory2",
        "value": "5555",
        "children": []
      }
    ]
  }
]
```

| Властивості | Опис |
|---|---|
| **Default data** | Задає структуру Accordion. `label` задає назву категорії, `value` задає значення, `children` дозволяє додавати підкатегорії.|
| **Scroll Contents** | Дозволяє використовувати скрол |
| **classStyle** | Дозволяє підключати стилі CSS |
| **Default selected item** | Вибирає елементи з вказаним значенням |

### Події 

Ви можете додавати поді написані на `JS` які будуть виконуватися при натисканні на елемент або обирати події з запропонованого списку 

| Подія | Опис |
|--|--|
| `onClickMain` | Викликається подія коли натискається головний елемент без вкладених елементів |
| `onClick` | Викликається подія коли натискається елемент |
| `onClickSub` | Викликається подія коли натискається вкладений елемент|

#### Список подій

Переглянути список подій які доступні можна в документації [Appsmith](https://docs.appsmith.com/reference/appsmith-framework/widget-actions)



