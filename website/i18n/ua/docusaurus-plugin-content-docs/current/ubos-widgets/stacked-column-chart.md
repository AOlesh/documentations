---
sidebar_position: 21
---

# Stacked column chart

Віджет **Stacked column chart** представляє собою aдаптивну складену горизонтальну гістограму з кількома стовпчиками, куди можна додавати маркери для позначення результатів або балів. За замовчуванням віджет **Stacked column chart** має три стовпчики та три маркери різного кольору.

Щоб додати віджет на канвас, перетягніть віджет **Stacked column chart** з панелі віджетів, розміщеної ліворуч.

![](./img/stacked-column-chart-1.png)

## Властивості

Властивості дозволяють редагувати віджет, з’єднувати його з іншими віджетами та налаштовувати дії користувача.

### Властивості віджета

Ці властивості дозволяють редагувати віджет **Stacked column chart**. Усі властивості присутні на панелі властивостей та перераховані у наведеній нижче таблиці.  

#### Chart Data
Віджет приймає масив об'єктів для відображення елементів в гістограмі. Можна отримати дані з існуючого `API` використовуючи `{{}}`

| Series Data | Опис |
|---|---|
| **`Series Title`** | Задає назву серії стовпців. Приймає дані типу `string`|
| **`x`** | Задає назву стовпця. Приймає дані типу `string` |
| **`y`** | Задає величину маркера яка буде відображена на стовпці. Приймає дані типу `number`. `Required` |
| **`seriesField`** | Задає назву маркера яка відображається в правому верхньому куті віджета. Приймає дані типу `string` |
|`Add Series` | Додає нову серію стовпців |
|`Animate Loading`| Анімація при завантаженні |
|`classStyle`| Задає CSS стилі|

![](./img/stacked-column-chart-2.png)

<details>
  <summary>Переглянути Series Data</summary>
  <div>

    [
        {
            "x": "2050-03-22",
            "y": 150,
            "seriesField": "field 1"
        },
        {
            "x": "2050-03-22",
            "y": 200,
            "seriesField": "field 1"
        },
        {
            "x": "2050-04-22",
            "y": 300,
            "seriesField": "field 1"
        },
        {
            "x": "2050-04-22",
            "y": 50,
            "seriesField": "field 2"
        },
        {
            "x": "2050-05-22",
            "y": 155,
            "seriesField": "field 2"
        },
        {
            "x": "2050-05-22",
            "y": 75,
            "seriesField": "field 3"
        },
        {
            "x": "2050-05-22",
            "y": 200,
            "seriesField": "field 3"
        }
    ]
    
  </div>
</details>