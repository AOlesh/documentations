---
sidebar_position: 1
---

# Getting started

:::info
Цей розділ допоможе вам охопити основи інструментів платформи UBOS, щоб швидко та легко створити власні проекти
:::

## Основні елементи UBOS

### Workspace
After you sign in to UBOS platform first screen that you will see is the home page with a list of Workspaces. By default there will be a basic workspace with the name of your account.

![ubos workspace](./img/getting-started-1.gif)

#### What is Workspace?

[Workspace](intro.md#workspace) - is a virtual space that allows you to gather various services, resources and work with them as a single unit. Currently in a workspace can be multiple services of Workflow manager, Databases and single application for User Interface.

![workspace items](./img/getting-started-2.png)

Let's start work on our amazing application in our default workspace. To open [workspace](intro.md#workspace) you need to click on edit button in the row of the selected workspace.

![workspace items](./img/getting-started-3.png)

### UI editor
Інтуїтивно зрозумілий візуальний інтерфейс із багатою бібліотекою віджетів React.js. Дайте розробникам можливість швидко створювати різні представлення та розгортати їх в ОДИН клік без знання коду за допомогою drag&drop.
![ui editor](https://www.ubos.tech/assets/images/ubos/uiEditor.gif)

### Flow editor
Підключайтеся до будь-якого джерела даних, наприклад до API та сторонніх додатків, баз даних. Візуалізуйте та виконуйте різні потоки інтеграції за допомогою одного інструменту. Забезпечте високу видимість для всієї логіки системи/сервера та зробіть швидку інтеграцію з будь-якими застарілими чи зовнішніми системами за допомогою широкого спектру протоколів зв’язку (HTTP, SOAP, Socket ...).
![flow editor](https://www.ubos.tech/assets/images/ubos/flowEditor.gif)

### Database manager
Створіть готову службу бази даних за допомогою кількох кліків і без коду. Базу даних буде автоматично налаштовано та встановлено в кластері Kubernetes у вашому просторі імен. 
Підтримувані бази даних:
- MongoDB
- Postgres
- MySQL
![database manager](https://www.ubos.tech/assets/images/ubos/databaseEditor.gif)

### Layout manager
Керуйте кількома  low-code редакторами в одній робочій області та перемикайтеся між ними в режимі реального часу без перезавантаження
![layout manager](https://www.ubos.tech/assets/images/ubos/layoutManager.gif)

