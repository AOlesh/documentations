---
sidebar_position: 8
---


# Взаємодія Flow Builder та UI Editor

Унікальною особливістю платформи UBOS є можливість створювати FrontEnd та BackEnd частини вашого проекту в просторі одного Workspace

Взаємодіяти між різними чатинами вашого проекту відбувається наступним чином:

`Flow Editor` дає вам можливість створювати `EndPoint` в яких відображати потужні робочі процеси, що можуть інтегрувати широкий спектр зовнішніх систем і створювати складну логіку для вашої програми. 

Використовувати дані `EndPoint` 's можна при створенні `API` в `UI Editor` 

![](./img/flow-builder-ui-editor-2.gif)