---
slug: mdx-blog-post
title: Жарт який демонструє можливості MDX для Blog Post
authors: [myron]
tags: [docusaurus]
---

Підтримка дописів у блозі [Docusaurus Markdown features](https://docusaurus.io/docs/markdown-features), як от [MDX](https://mdxjs.com/).

:::tip

Це жартівливий допис для демонстрації можливостей mdx. Використовуйте безкоштовні поради для досягнення своїх цілей

```js
Натисніть кнопку щоб отримати пораду, що вам потрібно для розвитку вашого бізнесу
```

<button onClick={() => alert('only UBOS is what you need!')}>Click me!</button>

:::
