---
slug: ubos-lecture
title: UBOS lecture for Hochschule München
authors: [andrii, yurii]
tags: [low-code, no-code, ubos]
---

У нас не було багато часу, щоб розповісти вам про це перед поїздкою на [Web Summit](https://websummit.com/). 
Наприкінці жовтня ми мали можливість поділитися своїми знаннями та досвідом зі студентами Мюнхенського університету прикладних наук – [Hochschule München](https://www.hm.edu/index.de.html) 
Команда UBOS підготувала лекцію `«Low Code technology for Edge AI and IoT case»`. 
Дякуємо університету за можливість поділитися своїми напрацюваннями зі студентами. Ми впевнені, що наші знання будуть їм у нагоді!

![UBOS lecture](./ubos_lecture.jpg)
