---
sidebar_position: 2
---

# Workspace
As we spoke previously [workspace](intro.md#workspace) is a unit that gathers all your services/resources which are used during the application development process.

Here is a default view of the workspace when you open it for the first time.

![workspace](./img/getting-started-4.png)

1. `Main work zone` - Here will be opened each visual editor service which you will use during your application development. You can open simultaneously as many editors as you like and change the size of each editor by our Layout manager.
2. `Service manager` - Used to browse, open, and manage all of the services in your project. UBOS is a tool based low-code platform - you can get started immediately by opening UI Editor, create new Flow Manager (Node-red) or establish new Database service for your application.
3. `Activity bar` - Located on the far left-hand side, this lets you switch between global views in the UBOS platform.
4. `Top bar` - Displays overall information about your opened tabs..