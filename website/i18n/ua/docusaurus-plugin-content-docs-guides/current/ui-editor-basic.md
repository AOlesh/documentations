---
sidebar_position: 3
---

# UI Editor - basic

## UI Editor Layout
Let's start from exploring the first tool in the UBOS ecosystem which will help us to build Dashboards, Admin panels, CRUD applications and other type of web apps faster and with almost no coding.

To open visual editor we need to click on the **UI Editor** menu on the **Service** manager

![](./img/ui-editor-basic-1.gif)

After the click **UI Editor** will be opened in **Main work zone**.

![](./img/ui-editor-basic-1.png)

**UI Editor** has own layout which contain 4 main windows:

1. **Sidebar** - Helps you to create and organize Pages, UI Widgets and API for data integration
2. **Page Editor** - This is the canvas where you can drag UI Widgets from the Widget pane and design how you page will looks. Each page has own canvas.
3. **Widget Settings** - Shows the properties of the selected UI Widget on canvas
4. **Header** - Helps to import and export application, edit page name, and also performs deployment

## Widgets Pane
The **Widget pane** can be opened by clicking on the **Widgets** button inside the current **Page**. After the pane will be opened you will see all available widgets that you can drag&drop to the page canvas for designing your page

![](./img/ui-editor-basic-2.gif)

## Adding UI Widgets
Each widget from the widget pane can be dragged on the page canvas to build the **UI**. After the widget is placed on page canvas it can be resized to fit into your page design and data that need to be displayed.
![](./img/ui-editor-basic-3.gif)

## Widget Settings
Each widget has a list of properties that can be configured in the **Widget Settings** window on the right side of the **UI Editor**. To open the **Widget Settings** window you just need to click on the label on top of the widget. Widget properties can be used to change widget `style`, `data`, or `action`.
![](./img/ui-editor-basic-4.gif)

Also, widget properties can be dynamic values. To make it dynamic you need to type {{ }} inside the widget property and it to another Widgets or API. 

## Deploying UI
Let's create a sample page that consists of 3 Button widgets and 1 Table widget. Try to configure widgets on the page accordingly to the example below:

![](./img/ui-editor-basic-2.png)

When the page is ready you need to deploy it to see it from the side of the end-user, to do that click on the **Deploy** button on the `Top Bar`, on the modal window that will appear after, click on **View Application** and a new tab will be opened in your browser with the preview URL of your application.

![](./img/ui-editor-basic-5.gif)

## Working with API

To make your app dynamic you need to have some data, you can get that data by creating an API. You can visualize any data received from API call by set widgets:

<table>
  <thead>
    <tr>
      <th>Widget</th>
      <th>Property</th>
      <th>Data Type</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Text</td>
      <td>Text</td>
      <td>String</td>
    </tr>
    <tr>
      <td>Table</td>
      <td>Table data</td>
      <td>Array of objects</td>
    </tr>
        <tr>
      <td>Chart</td>
      <td>Chart Data</td>
      <td>Array of (x,y)</td>
    </tr>
        <tr>
      <td>Image</td>
      <td>Image</td>
      <td>URL / Base64</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <td>Dropdown</td>
      <td>$Options</td>
      <td>Array of (label, value)</td>
    </tr>
  </tfoot>
</table>

You can create new API by clicking on the + button in the sidebar menu inside current Page. After the click, you should see the New Blank API button after then you should see the API pane where you can edit API properties

![](./img/ui-editor-basic-6.gif)

### API Pane 

Here you can modify your API by REST interface. API supports all REST HTTP methods GET,POST,PUT,DELETE,PATCH and here you can configure values of header, body fields and params.

![](./img/ui-editor-basic-3.png)



 