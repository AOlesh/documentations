---
sidebar_position: 1
---

# Getting started

:::info

The Guides section will help you to cover the basics of the UBOS tools available to help you create an application fast and easy
:::

## Workspace
After you sign in to UBOS platform first screen that you will see is the home page with a list of Workspaces. By default there will be a basic workspace with the name of your account.

![ubos workspace](./img/getting-started-1.gif)

### What is Workspace?

[Workspace](intro.md#workspace) - is a virtual space that allows you to gather various services, resources and work with them as a single unit. Currently in a workspace can be multiple services of Workflow manager, Databases and single application for User Interface.

![workspace items](./img/getting-started-2.png)

Let's start work on our amazing application in our default workspace. To open [workspace](intro.md#workspace) you need to click on edit button in the row of the selected workspace.

![workspace items](./img/getting-started-3.png)