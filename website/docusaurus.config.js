// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Single low-code platform to power your innovation',
  tagline: 'UBOS aims to empower anyone to build applications by hiding the complexity of the software development lifecycle',
  url: 'https://aolesh.gitlab.io',
  baseUrl: '/documentations',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'ubos', // Usually your GitHub org/user name.
  projectName: 'documentations', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'uk'],
    localeConfigs: {
      en: {
        label: 'English',
        direction: 'ltr',
        htmlLang: 'en-US',
        calendar: 'gregory',
        path: 'en',
      },
      uk: {
        label: 'Українська',
        direction: 'ltr',
        htmlLang: 'uk-UA',
        calendar: 'gregory',
        path: 'ua',
      },
    },
  },
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: 'docs',
          path: 'docs',
          sidebarPath: require.resolve('./sidebars.js'),
          lastVersion: 'current',
          onlyIncludeVersions: ['current'],
          remarkPlugins: [math],
          rehypePlugins: [katex],
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/betedo_dev/ubosproject/documentations/-/tree/main/website',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/betedo_dev/ubosproject/documentations/-/tree/main/website',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],
  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'guides',
        path: 'guides',
        routeBasePath: 'guides',
        sidebarPath: require.resolve('./sidebars.js'),
        editUrl:
            'https://gitlab.com/betedo_dev/ubosproject/documentations/-/tree/main/website',
      }, 
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'UBOS',
        logo: {
          alt: 'UBOS Logo',
          src: 'img/logo.svg',
        },
        items: [
          // {
          //   to: '/guides/intro',    // ./docs-api/Intro.md
          //   label: 'Guides',
          //   position: 'left',
          //   activeBaseRegex: `/getting-started/`,
          // },
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Documentations',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://platform.ubos.tech/',
            label: 'Platform',
            position: 'right',
          },
          {
            type: 'localeDropdown',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'UBOS community',
                href: 'https://community.ubos.tech/',
              },
              {
                label: 'Documentations',
                to: '/docs/intro',
              },
              {
                label: 'Blog',
                to: '/blog',
              },
            ],
          },
          {
            title: 'Social',
            items: [
              {
                label: 'Instagram',
                href: 'https://www.instagram.com/ubos.tech/',
              },
              {
                label: 'Facebook',
                href: 'https://www.facebook.com/ubos.tech',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/UBOS_tech',
              },

            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Terms of service',
                href: 'https://ubraine.com/wp-content/uploads/2018/09/Privacy-and-Cookies.pdf'
              },
              {
                label: 'Privacy policy',
                href: 'https://ubraine.com/wp-content/uploads/2018/09/Privacy-and-Cookies.pdf',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} UBOS`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
