---
slug: what-is-low-code
title: What is low-code?
authors: yevhen

tags: [low-code, engineering]
---

Low-code is a software development approach that requires little to no coding in order to build applications and processes.

It involves using tools with an intuitive, drag-and-drop visual interface to create a unique solution to a problem instead of extensive coding languages. The resulting solution can take many forms—from building mobile, voice, or ecommerce apps and websites to automating any number of tasks or processes.
