---
slug: ubos-lecture
title: UBOS lecture for Hochschule München
authors: [andrii, yurii]
tags: [low-code, no-code, ubos]
---

We didn't have much time to tell you before the [Web Summit](https://websummit.com/) trip🚀. 
At the end of October, we had the opportunity to share our knowledge and experience with the Munich University of Applied Sciences – [Hochschule München](https://www.hm.edu/index.de.html) - students🏫
The UBOS team prepared the lecture  `«Low Code technology for Edge AI and IoT case»`. 
We thank the university for the opportunity to share our work with students. We are sure that our knowledge will be useful to them!

![UBOS lecture](./ubos_lecture.jpg)
