---
slug: mdx-blog-post
title: A joke that demonstrates the capabilities of MDX Blog Post
authors: [myron]
tags: [docusaurus]
---

Blog posts support [Docusaurus Markdown features](https://docusaurus.io/docs/markdown-features), such as [MDX](https://mdxjs.com/).

:::tip

Use free tips to reach your planned goal faster

```js
Click the button to find out what will accelerate the development of your business
```

<button onClick={() => alert('only UBOS is what you need!')}>Click me!</button>

:::
