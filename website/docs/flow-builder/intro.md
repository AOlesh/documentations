---
sidebar_position: 1
---

# Flow Builder

Flow Builder is based on Node-Red

Node-RED is a programming tool for wiring together APIs and online services in new and interesting ways.

It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click.

Learn more about Node-Red here:

<iframe src="https://nodered.org/docs/" width="1280" height="1500" title="Node Red Documentation"></iframe>
