---
sidebar_position: 1
---

# Version Control With Git

Version Control with Git allows you to version control your apps by connecting them with a Git hosting provider (Github, GitLab, Bitbucket), making it easier to track changes, create rollbacks or collaborate using git branches.