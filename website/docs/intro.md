---
sidebar_position: 1
---

# Getting started

:::info
That section will help you to cover the basics of the UBOS tools available to help you create an application fast and easy
:::

## Basic elements of UBOS

### Workspace
After you sign in to UBOS platform first screen that you will see is the home page with a list of Workspaces. By default there will be a basic workspace with the name of your account.

![ubos workspace](./img/getting-started-1.gif)

#### What is Workspace?

[Workspace](intro.md#workspace) - is a virtual space that allows you to gather various services, resources and work with them as a single unit. Currently in a workspace can be multiple services of Workflow manager, Databases and single application for User Interface.

![workspace items](./img/getting-started-2.png)

Let's start work on our amazing application in our default workspace. To open [workspace](intro.md#workspace) you need to click on edit button in the row of the selected workspace.

![workspace items](./img/getting-started-3.png)

## UI editor
An intuitive visual drag&drop interface with a rich library of React.js widgets. Give developers the ability to quickly create different views and deploy them in ONE click without any knowledge of the code.
![ui editor](https://www.ubos.tech/assets/images/ubos/uiEditor.gif)

## Flow editor
Connect to any data source such as API and third party apps, Databases. Visualize and execute different integration flows using a single tool. Provide high visibility for all the system/server logic and make fast integration with any legacy or external systems by using a wide range of communication protocols (HTTP, SOAP, Socket ...).
![flow editor](https://www.ubos.tech/assets/images/ubos/flowEditor.gif)

## Database manager
Сreate a production-ready database service with a couple of clicks and no-code. The database will be automatically configured and established in the Kubernetes cluster under your namespace.
Supported databases:
- MongoDB
- Postgres
- MySQL
![database manager](https://www.ubos.tech/assets/images/ubos/databaseEditor.gif)

## Layout manager
Manage multiple low-code editors into a single workspace and switch between in realtime with no reloading
![layout manager](https://www.ubos.tech/assets/images/ubos/layoutManager.gif)

