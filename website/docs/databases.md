---
sidebar_position: 5
---

# Databases

Most of the applications need to store data, in UBOS platform you can use an external database or set up your own database. To create a new database you need to open **Service Manager** and click on the + button on the **Database** row, after the modal window will be opened, there you can configure your new database.

![](https://docs.ubos.tech/uploads/images/gallery/2021-12/image-1639664270525.gif)

#### Supported databases:

- MongoDB
- Postgres
- MySQL

:::info
External access to the database - allow you to connect to your database from external client or system
:::

After you fill in all required fields, click on Create Database button and your new database service will be created in couple of seconds. Your new database service will be avilable under Database dropdown (click on it and you will see the list)

![](https://docs.ubos.tech/uploads/images/gallery/2021-12/image-1639664722595.gif)

To check the database information, open the list of available databases and click on it, modal window will be opened with all necessary information. 

![](https://docs.ubos.tech/uploads/images/gallery/2021-12/image-1639666486919.gif)